package com.visitpass.visitpass;

import com.google.gson.annotations.SerializedName;

import java.security.acl.Group;
import java.util.ArrayList;

/**
 * Created by haerul on 17/03/18.
 */

public class Contact {

    @SerializedName("id") private int Id;
    @SerializedName("fname") private String Name;
    @SerializedName("sname") private String Sname;
    @SerializedName("qrcode") private String Qrcode;
    @SerializedName("time") private String Time;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name + " " + Sname;
    }

    public String getEmail() {
        return Qrcode;
    }
    public String getTime() {
        return Time;
    }

}
