package com.visitpass.visitpass;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "mysharedpref12";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String TOKEN_TYPE = "token_type";
    private static final String EXPIRES_DATE = "expires_date";
    private static final String LIST_HISTORY = "list_history";
    private static final String NAME_USER = "name_user";
    private static final String AVATAR_USER = "avatar_user";
    private static final String OFFICE_USER = "office_user";
    private static final String ADDRESS_USER = "address_user";

    List<String> arrPackage;

    private SharedPrefManager(Context context){
        mCtx=context;
    }

    public static synchronized SharedPrefManager getmInstance(Context context){
        if(mInstance==null){
            mInstance=new SharedPrefManager(context);
        }
        return mInstance;
    }

    public boolean userLogin(String access_token,String refresh_token,String type_token,String expires_date){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(ACCESS_TOKEN, access_token);
        editor.putString(REFRESH_TOKEN, refresh_token);
        editor.putString(TOKEN_TYPE, type_token);
        editor.putString(EXPIRES_DATE, expires_date);
        editor.apply();

        return true;
    }

    public boolean userProfile(String name,String avatar,String office, String address){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(NAME_USER, name);
        editor.putString(AVATAR_USER, avatar);
        editor.putString(OFFICE_USER, office);
        editor.putString(ADDRESS_USER, address);
        editor.apply();

        return true;
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedPreferences.getString(ACCESS_TOKEN,null)!=null){
            return true;
        }
        return false;
    }

    public boolean logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }
    public boolean logout_for_new(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(ACCESS_TOKEN).commit();
        sharedPreferences.edit().remove(REFRESH_TOKEN).commit();
        sharedPreferences.edit().remove(TOKEN_TYPE).commit();
        sharedPreferences.edit().remove(EXPIRES_DATE).commit();
        return true;
    }

    /* GETTERS */

    public String getAccesToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(ACCESS_TOKEN,null);
    }

    public String getRefreshToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(REFRESH_TOKEN,null);
    }

    public String getTokenType(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN_TYPE,null);
    }

    public String getNameUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(NAME_USER,null);
    }

    public String getAvatarUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(AVATAR_USER,null);
    }

    public String getOfficeUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(OFFICE_USER,null);
    }

    public String getAddressUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(ADDRESS_USER,null);
    }

    /* END_GETTERS */



    public boolean isSetHistory(ArrayList arrayList){

        arrPackage = new ArrayList<String>(arrayList);
        if(arrPackage==null)
            return true;
        return false;

    }




    /*    public String getListHistory(){
            SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
            return  sharedPreferences.getString(LIST_HISTORY,null);

        }
    */
    public void packagesharedPreferences(String responseString) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();

        Gson gson = new Gson();
        //String json = gson.toJson(responseString);
        editor.putString(LIST_HISTORY,responseString);
        editor.apply();
        //Log.d("STORE/FROM/PREFERENCE",""+responseString);
    }

    public String loadDataHistory() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        Gson gson=new Gson();
        String json = sharedPreferences.getString(LIST_HISTORY, null);
        Type type = new TypeToken<ArrayList>(){}.getType();
       // Log.d("loadedHistory",""+json);
        return  json;
    }

    public void removeFromSharedPreferences(String key) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(key).commit();
        //Log.d("ddd","LIST_HISTORY is cleared");

    }
}
