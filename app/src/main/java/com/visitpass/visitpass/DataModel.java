package com.visitpass.visitpass;

public class DataModel {
    String data;
    String name;
    String qrcode;

    public DataModel(String name, String qrcode, String data) {
        this.name = name;
        this.qrcode = qrcode;
        this.data = data;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.data;
    }

    public String getVersion_number() {
        return this.data;
    }
}
