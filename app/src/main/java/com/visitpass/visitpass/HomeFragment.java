package com.visitpass.visitpass;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HomeFragment extends Fragment {

    SurfaceView surfaceView;
    CameraSource cameraSource;
    TextView textView;
    View rootView;

    DashboardFragment dashboardFragment;
    private ArrayList<String> songList;

    private int lastPage,currentPage=0;

    private JSONArray jsonArray;

    private static final String JSON_URL = "http://192.168.150.100/api/hist";

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home,
                container, false);
        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.imageButton);

        textView = (TextView) rootView.findViewById(R.id.textView);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Place a QRcode inside the rectangle to scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });
        //SharedPrefManager.getmInstance(getActivity()).removeFromSharedPreferences("list_history");
        String array = SharedPrefManager.getmInstance(getActivity()).loadDataHistory();
        if(array==null)
            sendHttpRequest();
        //else Snackbar.make(rootView.findViewById(R.id.fl_home),array,Snackbar.LENGTH_LONG).show();

        return rootView;
    }

    public boolean sendHttpRequest(){

        String t_token = SharedPrefManager.getmInstance(getActivity()).getTokenType();
        String a_token = SharedPrefManager.getmInstance(getActivity()).getAccesToken();
        String r_token = SharedPrefManager.getmInstance(getActivity()).getRefreshToken();

        OkHttpClient client = new OkHttpClient();


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                //.addFormDataPart("qrcode", qrCode)
                .addFormDataPart("refresh_token", r_token)
                .build();

        final Request request = new Request.Builder()
                .url(JSON_URL)
                .header("Authorization", t_token + " " + a_token)
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Snackbar.make(rootView.findViewById(R.id.fl_dashboard), "Unexpecred Error Occured: " + e.getMessage(),Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String myResponse = response.body().string();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getJsonHandler(myResponse);
                        }

                    });
                }else{
                    Snackbar.make(rootView.findViewById(R.id.fl_dashboard), "Unexpecred Error Occured: ",Snackbar.LENGTH_LONG).show();
                }
            }
        });

        return false;
    }

    private void getJsonHandler(String myResponse) {
        songList = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(myResponse);
            if (obj.has("hitory")) {
                jsonArray = obj.getJSONArray("hitory");
                SharedPrefManager.getmInstance(getActivity()).packagesharedPreferences(jsonArray.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}