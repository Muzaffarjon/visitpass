package com.visitpass.visitpass;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnClickListener {


    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */

    // UI references.
    private String http = "http://192.168.150.100/api/login";
    private boolean check = false;
    private EditText mPasswordView, mUserName;
    private TextView mTextResult;
    private View mProgressView;
    private View mLoginFormView;
    private ProgressDialog progressDialog;
    private Button btnLogin;

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(SharedPrefManager.getmInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this,MainActivity.class));
            return;
        }

        dialog= new ProgressDialog(LoginActivity.this);

        dialog.setMessage("Please wait ...");
        // Set up the login form.
        mUserName = (EditText) findViewById(R.id.txtUserName);

        mPasswordView = (EditText) findViewById(R.id.txtPassword);
        mTextResult = findViewById(R.id.content);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");

        btnLogin = (Button) findViewById(R.id.bntLogin);
        btnLogin.setOnClickListener(this);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }


    private boolean functionGetRequest(String alogin,String password) {

        OkHttpClient client = new OkHttpClient();
        String url = http;
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", alogin)
                .addFormDataPart("password", password)
                .build();
        final Request request = new Request.Builder()
                .url(url)
                .post(requestBody)

                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(LoginActivity.this, "ERROR!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {
                    final String myResponse = response.body().string();
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject obj = new JSONObject(myResponse);
                                Boolean list = Arrays.asList(obj).contains("error");
                                if (obj.getBoolean("error") == true) {

                                    mUserName.setError("Username or Password is incorrect!");
                                    dialog.dismiss();
                                }
                                if (obj.getString("access_token") != null && obj.getString("token_type") != null) {
                                    SharedPrefManager.getmInstance(getApplicationContext()).userLogin(
                                            obj.getString("access_token"),
                                            obj.getString("refresh_token"),
                                            obj.getString("token_type"),
                                            obj.getString("expires_in")


                                    );
                                    SharedPrefManager.getmInstance(getApplicationContext()).userProfile(
                                            obj.getJSONObject("data").getJSONObject("user").getString("name"),
                                            obj.getJSONObject("data").getJSONObject("user").getString("avatar"),
                                            obj.getJSONObject("data").getJSONObject("user").getString("office"),
                                            obj.getJSONObject("data").getJSONObject("user").getString("address")
                                    );
                                    functionClearForm(mUserName);
                                    functionClearForm(mPasswordView);
                                    dialog.dismiss();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
                }else{
                    switch (response.code()) {
                        case 200:
                            Toast.makeText(LoginActivity.this, "nofff", Toast.LENGTH_SHORT).show();
                        case 404:
                            Toast.makeText(LoginActivity.this, "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(LoginActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                            break;

                    }
                }

            }

        });

        return check;

    }
    @Override
    public void onClick(View view) {
        String alogin = mUserName.getText().toString();
        String password = mPasswordView.getText().toString();
        dialog.show();
        if (TextUtils.isEmpty(alogin) && TextUtils.isEmpty(password)) {
            mUserName.setError("Please fill Username!");
            mPasswordView.setError("Please fill password!");
            dialog.dismiss();
        }else if(TextUtils.isEmpty(alogin)){
            mUserName.setError("Please fill Username!");
            dialog.dismiss();
        }else if(TextUtils.isEmpty(password)){
            mPasswordView.setError("Please fill password!");
            dialog.dismiss();
        }else {
            if (view == btnLogin) {
                functionGetRequest(alogin, password);
            }
        }
    }

    private void functionClearForm(EditText mPasswordView) {
        mPasswordView.getText().clear();
    }
}

