package com.visitpass.visitpass;

import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    //@GET("/api/search")
    //Call<HashMap<String, List<Contact>>> getContactDate(@Query("input_type") String str, @Query("sdate") String str2, @Query("edate") String str3);

    @GET("/api/search")
    Call<HashMap<String,List<Contact>>> getContactDate(@Query("input_type") String str,@Query("sdate") String key);

    @GET("/api/search")
    Call<HashMap<String, List<Contact>>> getContactName(@Query("input_type") String str, @Query("fname") String str2);

    @GET("/api/search")
    Call<HashMap<String, List<Contact>>> getContactQrcode(@Query("input_type") String str, @Query("qrcode") String str2);


}
