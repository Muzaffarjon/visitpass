package com.visitpass.visitpass;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {
    private String mMessage;
    Handler mainHandler = new Handler(Looper.getMainLooper());
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = "myLogs";
    private TextView access_token;

    ProgressDialog dialog;

    private String http = "http://192.168.150.100/api/scan";
    HomeFragment homeFragment;
    TextView textView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(HomeFragment.newInstance());
                    return true;
                case R.id.navigation_dashboard:
                    loadFragment(DashboardFragment.newInstance());

                    return true;
            }
            return false;
        }
    };

    private boolean loadFragment(Fragment fragment) {
        if(fragment != null){

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_content,fragment).commit();
            return true;
        }
        return false;

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialog= new ProgressDialog(MainActivity.this);

        dialog.setMessage("Please wait ...");
        textView= (TextView) findViewById(R.id.textView);

        if(!SharedPrefManager.getmInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadFragment(new HomeFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.menuLogout:
                SharedPrefManager.getmInstance(this).logout();
                finish();
                startActivity(new Intent(this,LoginActivity.class));
                break;
            case R.id.search:
                overridePendingTransition(0, 0);
                startActivity(new Intent(this,SearchActivity.class));
                break;
            case R.id.profile:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MainActivity.this,ProfileActivity.class));
                break;
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment= new HomeFragment();
                break;
            case R.id.navigation_dashboard:
                fragment= new DashboardFragment();
                break;
        }

        return loadFragment(fragment);

    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null){
            if(result.getContents()==null){
                Toast.makeText(this,"You cancelled the scanning",Toast.LENGTH_LONG).show();
            }else{
                dialog.show();
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                sendHttpRequest(result.getContents());
                //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void sendHttpRequest(String qrCode){

        String t_token = SharedPrefManager.getmInstance(this).getTokenType();
        String a_token = SharedPrefManager.getmInstance(this).getAccesToken();
        String r_token = SharedPrefManager.getmInstance(this).getRefreshToken();
        OkHttpClient client = new OkHttpClient();


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("qrcode", qrCode)
                .addFormDataPart("refresh_token", r_token)
                .build();

        final Request request = new Request.Builder()
                .url(http)
                .header("Accept", "application/json")
                .header("Authorization", t_token + " " + a_token)
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            String myResponse;
            @Override
            public void onFailure(Call call, IOException e) {
                mMessage = e.toString();
                Log.d(TAG, mMessage); // no need inside run()
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(findViewById(R.id.fl_home), mMessage,Snackbar.LENGTH_LONG).show();

                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                myResponse = response.body().string();
                Log.d(TAG, myResponse); // no need inside run()
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(!getErrorFromRequest(myResponse)){

                            getJsonHandler(myResponse);

                        } // must be inside run()
                    }
                });
                switch (response.code()) {
                    case 401:
                        finishTheApp();
                        break;
                    case 500:
                        Snackbar.make(findViewById(R.id.fl_home), "server broken",Snackbar.LENGTH_LONG).show();
                        break;
                }

            }
        });
    }

    public void getJsonHandler(String myResponse) {
        try {
            JSONObject obj = new JSONObject(myResponse);
            String fname = obj.getJSONObject("client").getString("fname");
            String name = obj.getJSONObject("client").getString("sname");
            //Log.d(TAG,fname + " " + name);
            if(obj.getBoolean("error")==false) {
                refreshAccessToken(obj.getString("access_token"), obj.getString("refresh_token"), obj.getString("token_type"),obj.getString("expires_in"));
                Snackbar.make(findViewById(R.id.fl_home), "Scanned successful!", Snackbar.LENGTH_LONG).show();
                SharedPrefManager.getmInstance(this).removeFromSharedPreferences("list_history");
                dialog.dismiss();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                }, 1000);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refreshAccessToken(String a_token,String r_token, String t_type,String expires){
        Log.d(TAG,"I had WORK!");
        if(SharedPrefManager.getmInstance(this).logout_for_new())
            SharedPrefManager.getmInstance(getApplicationContext()).userLogin(
                    a_token,
                    r_token,
                    t_type,
                    expires

            );
        //Snackbar.make(findViewById(R.id.fl_home),"Access Refreshed",Snackbar.LENGTH_LONG).show();
    }

    private boolean getErrorFromRequest(String myResponse) {
        boolean check=false;
        try {
            JSONObject obj = new JSONObject(myResponse);
            String errors = obj.getJSONObject("errors").getString("qrcode");
            String message = obj.getString("message");
            Log.d(TAG,errors);
            if(errors.equals("Not limit")){
                Snackbar.make(findViewById(R.id.fl_home),message,Snackbar.LENGTH_LONG).show();
                dialog.dismiss();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                check=true;
            }else if(errors.equals("Not")){
                Snackbar.make(findViewById(R.id.fl_home),message,Snackbar.LENGTH_LONG).show();
                dialog.dismiss();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                check=true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return check;
    }

    private void finishTheApp(){
        SharedPrefManager.getmInstance(this).logout();
        finish();
        startActivity(new Intent(this,MainActivity.class));
    }
}
