package com.visitpass.visitpass;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    String cut_url = "http://newsforday.ru";
    CircleImageView imageView;
    TextView textAddress;
    TextView textOffice;
    TextView textView;
    String url = "/uploads/small/CHFN53Mca4DZVa09_tfZyNTbN-GCp72dFJ_L1wioD4aAnYHpjG1ZIxZL3Lul37dI.jpg";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        if (!SharedPrefManager.getmInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        imageView = (CircleImageView) findViewById(R.id.profile_image);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.cut_url);
        stringBuilder.append(this.url);
        loadImageFromUrl(stringBuilder.toString());
        textView = (TextView) findViewById(R.id.textView);
        textOffice = (TextView) findViewById(R.id.textOffice);
        textAddress = (TextView) findViewById(R.id.textAddress);
        textView.setText(SharedPrefManager.getmInstance(this).getNameUser());
        textOffice.setText(SharedPrefManager.getmInstance(this).getOfficeUser());
        textAddress.setText(SharedPrefManager.getmInstance(this).getAddressUser());
    }

    private void loadImageFromUrl(String url) {
        Picasso.with(this).load(url).placeholder(R.mipmap.ic_launcher).fit().error(R.mipmap.ic_launcher).into(this.imageView, new Callback() {
            public void onSuccess() {
            }

            public void onError() {
            }
        });
    }
}
