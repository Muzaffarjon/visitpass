package com.visitpass.visitpass;

import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Contact> contacts;
    private Adapter adapter;
    private ApiInterface apiInterface;
    ProgressBar progressBar;
    TextView search;
    Gson gson;
    String[] item;
    String isChecked="name";
    Call<HashMap<String,List<Contact>>> call;


    private String datePick;

    Handler mainHandler = new Handler(Looper.getMainLooper());

    private String mMessage;
    private String string;

    private RadioButton name,date,qrcode;

    private RadioGroup radioGroup;

    DatePickerDialog dialog2;

    private Menu menu;

    private static final String TAG="SearchActivity";

    private DatePickerDialog.OnDateSetListener mDataSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if(!SharedPrefManager.getmInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

        progressBar = findViewById(R.id.prograss);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        fetchContact("date",dateFormat.format(date),"");

        mDataSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String formatted=String.format("%d-%02d-%02d",year,month+1,dayOfMonth);
                datePick=year + "-" + (month+1) + "-" + dayOfMonth;
                Log.d(TAG,formatted);
                fetchContact(isChecked,formatted,"2019-01-07");

            }
        };

        //if(datePick!=null)
        //    fetchContact(isChecked,datePick);
    }

    public void fetchContact(String type, String key,@Nullable String key2){
        apiInterface = ApiClient.getApiClient(SharedPrefManager.getmInstance(this).getTokenType() + " " + SharedPrefManager.getmInstance(this).getAccesToken()).create(ApiInterface.class);
        switch(type){
            case "date" :
                call = apiInterface.getContactDate(type, key);
                break;
            case "name" :
                call = apiInterface.getContactName(type, key);
                break;
            case "qrcode" :
                call = apiInterface.getContactQrcode(type, key);
                break;
        }


        //call.enqueue(new Callback<List<Contact>>() {
        call.enqueue(new Callback<HashMap<String,List<Contact>>>() {
            @Override
            public void onResponse(Call<HashMap<String, List<Contact>>> call, Response<HashMap<String, List<Contact>>> response) {
                HashMap<String, List<Contact>> history=response.body();
                Log.d(TAG, String.valueOf(response.body()));

                if(!history.isEmpty()) {
                    contacts = new ArrayList<>(history.get("hitory"));
                    Log.d(TAG, contacts.toString());

                    progressBar.setVisibility(View.GONE);
                    //contacts=response.toString();
                    Log.d(TAG, response.toString());

                    adapter = new Adapter(contacts, SearchActivity.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }else Snackbar.make(findViewById(R.id.lt_search), "Nothing found", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<HashMap<String, List<Contact>>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                //Log.d("GHaa ka sk aksjdk j", contacts.toString());
                Toast.makeText(SearchActivity.this, "Nothing was found", Toast.LENGTH_LONG).show();
            }

        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.topmenu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchContact(isChecked, query,"");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               // fetchContact(isChecked, newText,"");
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.name_mode: item.setChecked(true);
                isChecked="name";
                break;
            case R.id.date_mode: item.setChecked(true);
                progressBar.setVisibility(View.VISIBLE);
                isChecked="date";
                Calendar cal= Calendar.getInstance();
                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        SearchActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDataSetListener,
                        year,month,day);
                dialog.setTitle("Chose the start date");

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                dialog2 = new DatePickerDialog(
                        SearchActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDataSetListener,
                        year,month,day);
                dialog2.setTitle("Chose the end of date");
                dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                break;
            case R.id.qrcode_mode: item.setChecked(true);
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Place a QRcode inside the rectangle to scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
                isChecked="qrcode";

                break;
            default:
                isChecked="name";
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {


        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if(result!=null){
            if(result.getContents()==null){
                Toast.makeText(this,"You cancelled the scanning",Toast.LENGTH_LONG).show();
            }else{
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                //        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                //sendHttpRequest(result.getContents());
                fetchContact(isChecked,result.getContents(),"");
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(Build.VERSION.SDK_INT > 11) {
            invalidateOptionsMenu();
            if(menu.findItem(R.id.name_mode).isChecked())
                isChecked= new String("name");
            //menu.findItem(R.id.name_mode).setVisible(false);
            //menu.findItem(R.id.date_mode).setVisible(false);
            //menu.findItem(R.id.qrcode_mode).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
