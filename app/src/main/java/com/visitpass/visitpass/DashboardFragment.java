package com.visitpass.visitpass;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    private ListView listView;
    private TextView textView;
    private Button next, previous;

    private ArrayList<DataModel> songList;
    private ArrayList<String> qrcode;
    View rootView;
    String array;

    private JSONArray jsonArray;

    private static CustomAdapter adapters;

    /* */

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private Adapter adapter; //adapters
    private ApiInterface apiInterface;
    Button button;
    ProgressBar progressBar;
    TextView search;
    String[] item;

    /* */

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }
    //Aleph0 adapter = new Aleph0();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        listView = rootView.findViewById(R.id.listView);
        button = rootView.findViewById(R.id.go_search);
        array = SharedPrefManager.getmInstance(getActivity()).loadDataHistory();
        if (array != null && !array.isEmpty())
            getListHistory();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView = rootView.findViewById(R.id.editText2);
                int i=0;
                List <DataModel> listClone = new ArrayList<DataModel>();
                ArrayList<Integer> ids = new ArrayList<>();
                for (DataModel string : songList) {
                    i++;
                    if(string.getName().matches("(?i)("+textView+").*")==true)
                    {
                        ids.add(i);
                        Log.d("DashBoard", String.valueOf(string.getName().matches("(?i)("+textView+").*")) + " position:" + i);
                    }


                }
                System.out.println(ids);

                listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
                listView.setSmoothScrollbarEnabled(true);
                listView.smoothScrollToPosition(17);


            }
        });

        return rootView;
    }


    private void getListHistory() {
        songList = new ArrayList<>();
        try {
            jsonArray = new JSONArray(SharedPrefManager.getmInstance(getActivity()).loadDataHistory());
            for (int count = 0; count < jsonArray.length(); count++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(count);
                    songList.add(new DataModel(jsonObject.optString("fname") + " " + jsonObject.optString("sname"), jsonObject.optString("qrcode"), jsonObject.getString("time")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            updateData();

            List<String> list = new ArrayList();
            list.add("behold");
            list.add("bend");
            list.add("bet");
            list.add("bear");
            list.add("beat");
            list.add("become");
            list.add("begin");



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void updateData() {
        adapters = new CustomAdapter(songList, getActivity());
        listView.setAdapter(adapters);

    }

}
